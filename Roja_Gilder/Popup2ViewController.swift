//
//  Popup2ViewController.swift
//  Gilder_For_Roja
//
//  Created by Shiksha Gurung on 5/2/20.
//  Copyright © 2020 vsrinu. All rights reserved.
//

import UIKit

class Popup2ViewController: UIViewController {
   
    
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var popView: UILabel!
    //button as a dropdown button.
    var button = dropDownbtn()
    // dropdown options to show the user
    var dropDownOptions: [String] = [BUS_STOPS[1].busStopName]
    // key used to distinguish between "home" and "university"
    var detailKey: String = "university"

    override func viewDidLoad() {
        super.viewDidLoad()
        popupView.layer.cornerRadius = 10
        //gives the exact shape that has been requested
        popupView.layer.masksToBounds = true
        //setting height,width of the button.
        button = dropDownbtn.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        
        // setting callerController for dropdownBtn to call
        button.callerController = self
        // Extract user home stop name or university stop name. If they don't exist use "Set Address" as default selection
        let title: String = UserDefaults.standard.string(forKey: self.detailKey) ?? "Set Address"
        button.setTitle(title, for: .normal)
        //setting the constraints
        //false because if we don't set it like that, the constraints we set down there just doesn't work.
        button.translatesAutoresizingMaskIntoConstraints = false
        //adds a button to our view.
        self.view.addSubview(button)
        //to position the button properly
        //centerXAnchor is going to center it on the x axis and centerYAnchor on Y axis.
        button.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        button.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        
        button.widthAnchor.constraint(equalToConstant: 200).isActive = true
        button.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        button.dropView.dropDownOptions = self.dropDownOptions
    }
    
    func storeValue(selected: String) {
        UserDefaults.standard.set(selected, forKey: self.detailKey)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        //dispose of any resources that can be received.
    }
    
    // Navigate to the page which called the popup controller. In this case, it would be the view controller.
    @IBAction func buttonClicked(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
}

protocol dropDownProtocol {
    func dropDownPressed(selected: String)
}

//creates dropdown menu (44 to 48)
class dropDownbtn: UIButton, dropDownProtocol{
    var dropView = dropDownView()
    var height = NSLayoutConstraint()
    var callerController: Popup2ViewController!
    
    func dropDownPressed(selected: String) {
        // set title for the button
        self.setTitle(selected, for: .normal)
        // store the value selected by the user. This will be stored to "home" if called from "Home" button, and stored in "university" if called from "University"
        self.callerController.storeValue(selected: selected)
        // close dropdowns.
        self.dismissDropDown()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.darkGray
        
        dropView = dropDownView.init(frame: CGRect.init(x: 0, y: 0, width: 0, height: 0))
        dropView.delegate = self
        dropView.translatesAutoresizingMaskIntoConstraints = false
    }
    
    override func didMoveToSuperview() {
        self.superview?.addSubview(dropView)
        self.superview?.bringSubviewToFront(dropView)
        dropView.topAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        dropView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        dropView.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
        height = dropView.heightAnchor.constraint(equalToConstant: 0)
    }
    
    var isOpen = false
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if isOpen == false {
            isOpen = true
            
            //we have to make sure that the button is deactivated at first so that the system doesnot clashes while its directly activated.
            NSLayoutConstraint.deactivate([self.height])
            
            self.height.constant = self.dropView.tableView.contentSize.height
            
            if self.dropView.tableView.contentSize.height > 150 {
                self.height.constant = 150
            }
            
            NSLayoutConstraint.activate([self.height])
            
            UIView.animate(withDuration: 0.5, delay: 0.5, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseInOut, animations: {
                //for animation.
                self.dropView.layoutIfNeeded()
                //if you start at y, that makes the drop down menu go down(+=), if at 00,if wanna go back up, you go -=.
                self.dropView.center.y += self.dropView.frame.height / 2
            }, completion: nil)
        } else {
            isOpen = false
            
            //we have to make sure that the button is deactivated at first so that the system doesnot clash while it's directly activated.
            NSLayoutConstraint.deactivate([self.height])
            self.height.constant = 0
            NSLayoutConstraint.activate([self.height])

            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseInOut, animations: {
                    //for animation.
                    self.dropView.layoutIfNeeded()
                    //we want it for it to go back up now. For this(-=),
                    self.dropView.center.y -= self.dropView.frame.height / 2
                },
                completion: nil
            )
        }
    }
    
    func dismissDropDown() {
        isOpen = false
        NSLayoutConstraint.deactivate([self.height])
        self.height.constant = 0
        NSLayoutConstraint.activate([self.height])

        UIView.animate(withDuration: 0.5, delay: 0.5, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseInOut, animations: {
                //for animation.
                self.dropView.layoutIfNeeded()
                //we want it for it to go back up now. For this(-=),
                self.dropView.center.y -= self.dropView.frame.height / 2
            },
            completion: nil
        )
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class dropDownView: UIView, UITableViewDelegate, UITableViewDataSource {
    //put all the options into this one variable which will fill the table view accordingly.
    
    var dropDownOptions = [String]()
    var tableView = UITableView()
    var delegate : dropDownProtocol!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        tableView.backgroundColor = UIColor.darkGray
        self.backgroundColor = UIColor.darkGray
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(tableView)
        
        tableView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //76-90 executor property.
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    //tells how many rows are there inside my tableview.
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dropDownOptions.count
    }
    
    //what kind of cell we are populating into our table view.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        
        cell.textLabel?.text = dropDownOptions[indexPath.row]
        cell.backgroundColor = UIColor.darkGray
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate.dropDownPressed(selected: dropDownOptions[indexPath.row])
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
}
