//
//  BusStopLocation.swift
//  Roja_Gilder
//
//  Created by vsrinu on 02/02/20.
//  Copyright © 2020 vsrinu. All rights reserved.
//

import Foundation


struct  BusStopLocation {
    
    let busStopName : String
    let busStopLongitude : Double
    let busStopLatitude : Double
}
let BUS_STOPS =  [
    BusStopLocation(busStopName: "Plenty Road", busStopLongitude: 145.0474, busStopLatitude: -37.7062),
               BusStopLocation(busStopName: "Thomas cherry building", busStopLongitude: 145.0465227, busStopLatitude: -37.7209443),
             BusStopLocation(busStopName: "David mayers building", busStopLongitude: 145.0462485, busStopLatitude: -37.7220602),
             BusStopLocation(busStopName: "Union building", busStopLongitude: 145.0474339, busStopLatitude: -37.7230981),
             BusStopLocation(busStopName: "Chisholm Collge", busStopLongitude: 144.8950346, busStopLatitude: -37.8535769),
             BusStopLocation(busStopName: "Glenn.Menzies college", busStopLongitude: 145.0486803, busStopLatitude: -37.7222579),
             BusStopLocation(busStopName: "Barnesway", busStopLongitude: 145.0500625, busStopLatitude: -37.7281948),
             BusStopLocation(busStopName: "Uni Lodge", busStopLongitude: 144.9822112, busStopLatitude: -37.7280341),
             BusStopLocation(busStopName: "Graduate house", busStopLongitude: 144.9591683, busStopLatitude: -37.8007354),
             BusStopLocation(busStopName: "Polaris", busStopLongitude: 145.0457632, busStopLatitude: -37.7120589)
        ]

           
