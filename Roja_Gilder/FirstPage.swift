//
//  FirstPage.swift
//  Gilder_For_Roja
//
//  Created by ROJA MUDUNDI on 3/2/20.
//  Copyright © 2020 vsrinu. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation


class ImageAnnotation : NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    var image: UIImage?
    var colour: UIColor?

    
    //give the default value of each property
    override init() {
        self.coordinate = CLLocationCoordinate2D()
        self.title = nil
        self.subtitle = nil
        self.image = nil
        self.colour = UIColor.white

        
        
        
        
    }
}


class ImageAnnotationView: MKAnnotationView {
   
    private var imageView: UIImageView!
    //private var means, you can only access to imageView within the class: ImageAnnotationView
    //private restricts the access. force people to only use the intended, and reduce error
    
    
    //this sets how the func MKannotationView should be strucuted
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        
        
        //setting properties
        self.frame = CGRect(x: 0, y: 0, width: 50, height: 50) //50*50 frame -- changing the width and height doesn't make any difference :(
        self.imageView = UIImageView(frame: CGRect(x: -10, y: -12, width: 50, height: 50))
        // The image is also 50*50 -- > the size of the image <
        
        //add this imageView to MKannotationView -- this is how you add the image to the annotation
        self.addSubview(self.imageView)

        //set the properties of the imageView
        self.imageView.layer.cornerRadius = 5.0
        
        self.imageView.layer.masksToBounds = true
        //masksToBounds: no idea what the fuck this is
    }

    
    //????? no very clear, but probably give the imageView
    override var image: UIImage? {
        get {
            return self.imageView.image
        }

        set {
            self.imageView.image = newValue
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}












class FirstPage: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var mymap: MKMapView!
    
     var locationManager = CLLocationManager()
    
    var locations = [["price": "$445", "title": "No.1 Plenty Road", "suburb": "0 Min" ,"lat" : -37.7173,
                            "long" : 145.0445, "url" : "https://geo2.ggpht.com/cbk?panoid=suYeogoCdVYW2uIvJM_PmA&output=thumbnail&cb_client=search.gws-prod.gps&thumb=2&w=408&h=240&yaw=321.79663&pitch=0&thumbfov=100"],
                          ["price": "$568","title": "No.2 Thomas Cherry Building", "suburb": "2 Min" ,"lat" : -37.7206,
          "long" : 145.0466, "url" :  "https://lh5.googleusercontent.com/p/AF1QipORIqsR_8UFHa0iizPrgq5wW-1HIpmbs-q5L2uw=w408-h240-k-no-pi-10-ya192-ro-0-fo100"],
                          ["price": "$360", "title": "No.3 Union Building", "suburb": "4 Min", "lat" : -37.7226,
                           "long" : 145.0490, "url" : "https://lh5.googleusercontent.com/p/AF1QipOpq3oDHviwi29EP0GXZ89M1p3YnGmOdhHUkeeM=w408-h306-k-no"],
                       ["price": "$390", "title": "No.4 Barnes Way", "suburb": "5 Min", "lat" : -37.7254,
                        "long" : 145.0503, "url" : "https://maps.gstatic.com/tactile/pane/default_geocode-2x.png"],
                       ["price": "$491", "title": "No.5 AgriBio Building", "suburb": "6 Min", "lat" : -37.7248,
                        "long" : 145.0545, "url" : "https://geo3.ggpht.com/cbk?panoid=bu5GrAbrho3nX7Ek7x5vGQ&output=thumbnail&cb_client=search.gws-prod.gps&thumb=2&w=408&h=240&yaw=307.5873&pitch=0&thumbfov=100"],["price": "$500", "title": "No.6 Terraces", "suburb": "6 Min", "lat" : -37.721881,
                                                                                                                                                                                                                     "long" : 145.0546646, "url" : "https://lh5.googleusercontent.com/p/AF1QipNOw1HW6IebRRchkDyn1dC0danetE8VmB4eocrY=w492-h240-k-no"],["price":"$500", "title": "No.7 Uni Lodge", "suburb": "10 Min", "lat" : -37.7233047,
                                                                                                                                                                                                                                                                                                                                                       "long" : 145.0599035, "url" : "https://lh5.googleusercontent.com/p/AF1QipPQ2Q6nHtaZiSzHOQ2OGloGAoA5YHEMoA2E0HHk=w408-h306-k-no"],["price": "$450", "title": "No.8 Gresswell", "suburb": "12 Min", "lat" : -37.7117773,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         "long" : 145.0620801, "url" : "https://maps.gstatic.com/tactile/pane/default_geocode-2x.png"], ["price": "$500", "title": "No.9 Graduate House", "suburb": "14 Min", "lat" : -37.7137785,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            "long" : 145.04848, "url" : "https://lh5.googleusercontent.com/p/AF1QipMVc5UsGg7hOJNtvVaABDLukdoPALbL7hK__KMH=w426-h240-k-no"],
       ["price": "$491", "title": "No.10 Polaris", "suburb": "17 Min", "lat" : -37.7121,
       "long" : 145.0475, "url" : "https://i.imgur.com/s6e6mQR.jpg"]]
    
    
    @IBOutlet weak var button: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mymap.delegate = self
                      locationManager.delegate = self
                      locationManager.desiredAccuracy = kCLLocationAccuracyBest
                      locationManager.requestWhenInUseAuthorization()
                      locationManager.startUpdatingLocation()
                      self.loadAnnotations()
        // Do any additional setup after loading the view.
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations location: [CLLocation]) {
    let location = CLLocationCoordinate2D(latitude: -37.7200, longitude: 145.0484)
    let span = MKCoordinateSpan(latitudeDelta: 0.015, longitudeDelta: 0.015)
    let region = MKCoordinateRegion(center: location, span: span)
    mymap.setRegion(region, animated: true)
    mymap.showsUserLocation = true
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
           if annotation.isKind(of: MKUserLocation.self) {  //Handle user location annotation..
               return nil  //Default is to let the system handle it.
           }

           if !annotation.isKind(of: ImageAnnotation.self) {  //Handle non-ImageAnnotations..
               var pinAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "DefaultPinView")
               if pinAnnotationView == nil {
                   pinAnnotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "DefaultPinView")
               }
               return pinAnnotationView
           }

           //Handle ImageAnnotations..
           var view: ImageAnnotationView? = mapView.dequeueReusableAnnotationView(withIdentifier: "imageAnnotation") as? ImageAnnotationView
           if view == nil {
               view = ImageAnnotationView(annotation: annotation, reuseIdentifier: "imageAnnotation")
           }

           let annotation = annotation as! ImageAnnotation
           view?.image = annotation.image
           view?.annotation = annotation
           view?.canShowCallout = true
           

           return view
       }
       
       func loadAnnotations() {

           for address in locations {
               
             
               
               
               DispatchQueue.main.async {
                   let request = NSMutableURLRequest(url: URL(string: address["url"] as! String)!)
                   request.httpMethod = "GET"
                   let session = URLSession(configuration: URLSessionConfiguration.default)
                   let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) in
                       if error == nil {

                           let annotation = ImageAnnotation()
                           annotation.coordinate = CLLocationCoordinate2D (latitude: address["lat"] as! CLLocationDegrees, longitude: address["long"] as! CLLocationDegrees)
                           
                           annotation.image = UIImage(data: data!, scale: UIScreen.main.scale)
                           
                           
                           annotation.title = address["title"] as? String
                           annotation.subtitle = address["suburb"] as? String
                           
                           
                           
                           DispatchQueue.main.async {
                               self.mymap.addAnnotation(annotation)
                           }
                       }
                   }

                   dataTask.resume()
               }
           }
       }
       
    
    
    
    
    
}

