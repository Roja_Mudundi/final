//
//  ViewController.swift
//  Roja_Gilder
//
//  Created by Roja Mudundi on 31/01/20.
//  Copyright © 2020 Roja Mudundi. All rights reserved.
//

import UIKit

import MapKit
import CoreLocation


class ViewController: UIViewController {
    
    
    @IBOutlet weak var setUniversity: UIButton!
    
    @IBOutlet weak var home: UIButton!
    
    @IBOutlet weak var university: UIButton!
    
    @IBOutlet weak var bottamSelectorsView: UIView!
    
    @IBOutlet weak var setHome: UIButton!
    @IBOutlet weak var homeView: UIView!
    @IBOutlet weak var universityView: UIView!
    
    @IBOutlet weak var searchBusStopLocationTxt: UITextField!
    @IBOutlet weak var busStopsInfoTbl: UITableView!
    
    
    @IBOutlet weak var testHomeButton: UILabel!
    
    
    var completeBusStopsInfoArr : [BusStopLocation]!
    
    
    @IBOutlet weak var userHelperMapView: MKMapView!
    let locationManager = CLLocationManager()
   
    let userCurrentLocationAnnotation = MKPointAnnotation()
    let userDestinationLocationAnnotation : MKPointAnnotation! = MKPointAnnotation()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
      
        
              userHelperMapView.delegate = self
              
            
        userHelperMapView.showsUserLocation = true
        
        
        completeBusStopsInfoArr = BUS_STOPS

        
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
            
            
//            var locValue = locationManager.location!.coordinate
////            locValue  = CLLocationCoordinate2D(latitude: CLLocationDegrees(floatLiteral: 37.437462) , longitude: CLLocationDegrees(floatLiteral: 48.448288))
//
//            userCurrentLocationAnnotation.coordinate = locValue
//            userCurrentLocationAnnotation.title = "Login User Name"
//            userCurrentLocationAnnotation.subtitle = "current location"
        }
        
        
       
        
        userHelperMapView.delegate = self
        userHelperMapView.mapType = .standard
        userHelperMapView.isZoomEnabled = true
        userHelperMapView.isScrollEnabled = true
        
        if let coor = userHelperMapView.userLocation.location?.coordinate{
            userHelperMapView.setCenter(coor, animated: true)
        }
        
        
        
        
        busStopsInfoTbl.layer.masksToBounds = true
        busStopsInfoTbl.layer.cornerRadius = 15
        busStopsInfoTbl.register(UITableViewCell.classForKeyedArchiver(), forCellReuseIdentifier: "busStopName")
        busStopsInfoTbl.tableFooterView = UIView.init(frame: .zero)
        
        
        bottamSelectorsView.layer.cornerRadius = 15
        bottamSelectorsView.layer.masksToBounds = true
    }
    
}

extension ViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return completeBusStopsInfoArr.count
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return  50
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let busStopInfoCell = tableView.dequeueReusableCell(withIdentifier: "busStopName", for: indexPath)
        busStopInfoCell.textLabel!.text = completeBusStopsInfoArr[indexPath.row].busStopName
        busStopInfoCell.backgroundColor = indexPath.row % 2 == 0 ? .gray : .white
        return busStopInfoCell
    }
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        busStopsInfoTbl.isHidden = true
        let row: BusStopLocation = completeBusStopsInfoArr[indexPath.row]
               let locValue = CLLocationCoordinate2D(latitude: CLLocationDegrees(floatLiteral: row.busStopLatitude), longitude: CLLocationDegrees(floatLiteral: row.busStopLongitude))
      
        
        userDestinationLocationAnnotation.coordinate = locValue
        userDestinationLocationAnnotation.title = completeBusStopsInfoArr[indexPath.row].busStopName
        userDestinationLocationAnnotation.subtitle = completeBusStopsInfoArr[indexPath.row].busStopName

        userHelperMapView.addAnnotation(userDestinationLocationAnnotation)

        self.showRouteOnMap()
        
    }
    // calls Popup2ViewController and shows the list of stops
    // shows all stops for home
    // shows just Thomas cherry building for university
    fileprivate func openPopup(key: String) {
        // extract storyboard
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        // find popView controller (identifier popupView in the storyboard as Popup2ViewController
        let popupController = storyBoard.instantiateViewController(withIdentifier: "popupView") as! Popup2ViewController
        // pass the names of all stops to the popupController
     popupController.dropDownOptions = key == "home" ? BUS_STOPS.map { $0.busStopName } : [BUS_STOPS[1].busStopName]
        // set detailKey, this is used to figure out what the user wants to store "home" or "university"
        popupController.detailKey = key
        // animate the popup view controller.
        self.present(popupController, animated: true, completion: nil)
    }
    
    
    // function called when "Home" button is pressed.
    
// extracts the lat and lon of "home" and drops a pin in that location
      
    
    // function called when "Univesity" button is pressed.
   // extracts the lat and lon of "university" and drops a pin in that location
      
    // function to extract out stored bus stop based on key. key can be "university" or "home"
    @IBAction func homeCheck(sender: AnyObject) {
        self.showAlert(key: "home")
    }
    
    @IBAction func universityCheck(sender: AnyObject) {
        self.showAlert(key: "university")
    }
    
    //asking the user about whether or not to set the uni / home address
    fileprivate func showAlert(key: String) {
        if (UserDefaults.standard.string(forKey: key) == nil) {
            let alert = UIAlertController(title: "Set \(key)", message: "Looks like you've not set the \(key) address. Would you like to set it now?", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { (action: UIAlertAction!) in
                self.openPopup(key: key)
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: {
                (action: UIAlertAction!) in
                _ = self.navigationController?.popViewController(animated: true)
            }))
            self.present(alert, animated: true, completion: nil)
        }
        else {
            self.storedBusStop(key: key)
        }
    }
    
    
    // The corresponding latitude and longitude of the stop is extracted from BUS_STOP list
    // Using that lat and lon, the map is re-centered and a pin is dropped in the corresponding location
    fileprivate func storedBusStop(key: String) -> Void {
        // get the second bus stop
        let secondBusStop = BUS_STOPS[1]
        // extract stored home or university address, if not found use first bus stop name
        let stopName: String = UserDefaults.standard.string(forKey: key) ?? secondBusStop.busStopName
        let busStop: BusStopLocation = BUS_STOPS.filter { $0.busStopName == stopName }[0]
        
        // create a span of 0.003 lat and 0.003 lon. 
        let span: MKCoordinateSpan = MKCoordinateSpan(latitudeDelta: 0.003, longitudeDelta: 0.003)
        // create coordinate object using bus stop lat lon
        let coordinate: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: busStop.busStopLatitude, longitude: busStop.busStopLongitude)
        // Create a region with bus stop coordinate in the center
        let region: MKCoordinateRegion = MKCoordinateRegion(center: coordinate, span: span)
        // navigate to the region
        self.userHelperMapView.setRegion(region, animated: true)
        // create an annotation (pin) and show it in the map.
        let annotation: MKPointAnnotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        self.userHelperMapView.addAnnotation(annotation)
        //pins name of the places on map
        annotation.title = busStop.busStopName
    }
}

extension ViewController : UITextFieldDelegate {
    
    @IBAction func textDidChanged(_ sender: Any) {
        
        let searchTxt = sender as! UITextField
        let searchString = searchTxt.text!
        if searchString.isEmpty {
            
            busStopsInfoTbl.isHidden = true
        }
        else {

        completeBusStopsInfoArr.removeAll()
        
//            print("=== \(self.getAllBusStopLocation())")
//        completeBusStopsInfoArr = self.getAllBusStopLocation().filter{$0.busStopName.lowercased().contains(searchTxt.text!)}
            completeBusStopsInfoArr = BUS_STOPS.filter{ $0.busStopName.range(of: searchString, options: .caseInsensitive, range: nil, locale: nil) != nil}
        busStopsInfoTbl.isHidden = false
        busStopsInfoTbl.reloadData()
        }
    }
}


extension ViewController :  CLLocationManagerDelegate, MKMapViewDelegate {
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        

        userHelperMapView.mapType = MKMapType.standard
        
        
        
        
     /*  userHelperMapView.delegate = self
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()*/
      
        userCurrentLocationAnnotation.coordinate = locValue
        userCurrentLocationAnnotation.title = "Login User Name"
        userCurrentLocationAnnotation.subtitle = "Current location"
        
        userHelperMapView.removeAnnotation(userCurrentLocationAnnotation)
        userHelperMapView.addAnnotation(userCurrentLocationAnnotation)
        
       
        
        let span = MKCoordinateSpan(latitudeDelta: 0.03, longitudeDelta: 0.03)
            let region = MKCoordinateRegion(center: locValue, span: span)
            userHelperMapView.setRegion(region, animated: true)
            
        
            
         
       
        
    }
    
    
    
    func showRouteOnMap() {
        let request = MKDirections.Request()
        request.source = MKMapItem(placemark: MKPlacemark(coordinate: userCurrentLocationAnnotation.coordinate, addressDictionary: nil))
        request.destination = MKMapItem(placemark: MKPlacemark(coordinate: userDestinationLocationAnnotation.coordinate, addressDictionary: nil))
        request.requestsAlternateRoutes = true
        request.transportType = .walking
        let directions = MKDirections(request: request)
        
        
        
        directions.calculate { [unowned self] response, error in
            guard let unwrappedResponse = response else { return }
            
            if (unwrappedResponse.routes.count > 0) {
                self.userHelperMapView.addOverlay(unwrappedResponse.routes[0].polyline)
            self.userHelperMapView.setVisibleMapRect(unwrappedResponse.routes[0].polyline.boundingMapRect, animated: true)
            }
        }
    }
    
    
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {

        let polylineRenderer = MKPolylineRenderer(overlay: overlay)
            polylineRenderer.strokeColor = UIColor.blue
            polylineRenderer.lineWidth = 5
        polylineRenderer.lineDashPattern = [0,10]
            return polylineRenderer
    }
    
    /*func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let annotationView = MKPinAnnotationView()
        
            
        
      
       
        if let des = userDestinationLocationAnnotation, annotationView.annotation?.title == des.title {
        
            annotationView.pinTintColor = .green
        }
        
        return annotationView
    }*/
}
